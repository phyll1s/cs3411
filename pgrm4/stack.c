#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "stack.h"

void init(Stack *s){
    s->capacity = 5;
    s->elements = malloc(s->capacity);
    s->top = -1;
}

//Return -1 for fail
//Add new element to stack - double capacity if need be
int push(Stack *s, char* toAdd){
    //printf("%s\n",toAdd);
    
    //If stack is full doulbe capacity
    if(s->top+1 == s->capacity){
        int newCap = s->capacity*2;
        
        char* temp = malloc(sizeof(s->elements));
        if(temp == NULL)
            return -1;
        strcpy(temp,*(s->elements));
        
        *(s->elements) = malloc(newCap * sizeof(char*));
	    if(s->elements == NULL)
	        return -1;
        strcpy(*(s->elements),temp);
        free(temp);
        s->capacity = newCap;
    }
    
    //Push new element to top
    s->top++;
    int tst = s->top;
    s->elements[s->top] = malloc(sizeof(toAdd)+1);
    if(s->elements[tst] == NULL)
        return -1;
    char* test1 = malloc(5*sizeof(char*));
    strcpy(test1,toAdd);
    //s->elements[s->top]
    return 1;
}

//Returns NULL on error
//Remove and set top element
char* pop(Stack *s){
    //If there's nothing in the stack
    if(s->top==-1)
        return NULL;
        
    //backup top
    char* toReturn = malloc(sizeof(s->elements[s->top]));
    if(toReturn == NULL)
        return NULL;
    strcpy(toReturn,s->elements[s->top]);
    
    free(s->elements[s->top]);
    s->top--;
    
    return toReturn;
}

//Return value of top element
char* top(Stack *s){
    //If there's nothing in the stack
    if(s->top==-1)
        return NULL;
    
    //Return top
    return s->elements[s->top];
}

//1 - empty
//0 - not-empty
int isEmpty(Stack *s){
    return s->top==-1;
}
