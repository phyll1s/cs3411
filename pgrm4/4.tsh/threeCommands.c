#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

main(){

  int apipe[2];
  int isParent;
  char *cmd[3][3];
  int i;
  int lastChild;
  int fd;
  int newOut;

  // 
  //---- Execute "sort < input  | cat | head -5 "
  //
  //  
  cmd[0][0] = "sort";
  cmd[0][1] = NULL;
  cmd[1][0] = "cat";
  cmd[1][1] = NULL;
  cmd[2][0] = "head";
  cmd[2][1] = "-5";
  cmd[2][2] = NULL;

  newOut=dup(1);
  for (i=2;i>=0;i--){
    pipe(apipe);
    isParent=fork();
    if (!isParent){ 
      close(apipe[1]);
      close(newOut);
      close(0);
      if (i != 0){ dup(apipe[0]); }
      if (i == 0) {  
             fd = open("input",O_RDONLY);
             dup(fd);
       }
      close(apipe[0]);
      execvp(cmd[i][0],cmd[i]);
      exit(1);
    } else {
      if (i==2) lastChild=isParent;
      close(apipe[0]);
      close(1);
      if (i!=0){dup(apipe[1]);}
      close(apipe[1]);
      if (i==0){dup2(newOut,1);waitpid(lastChild,NULL,0);}
    }//end if is-Parent
  }//end for
}
