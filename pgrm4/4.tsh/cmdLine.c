/*
 *  This code is only intended to illustrate use of standard library functions for routine command line parsing.
 *  (It does not adhere to all the coding guidelines required for project submissions.)
 *
 */

#include <stdio.h>
#include <string.h>

#define MAXCMDLENGTH 120
#define MAXCMDS       20
#define MAXARGS       20

main()
{
  char cmd[MAXCMDLENGTH];
  char prompt[]="tsh#";
  char *aCmd[MAXCMDS][MAXARGS];
  int  cmdCounter, i, nargs, tooLong;
  char *nextCmd;

  while (1==1){      
       //----- Write prompt
       //      Get command string  
       //      (Using "fgets" to avoid buffer overflow problems)
       //
       //      !!! Minimal error checking
       //
       tooLong=0;
       fputs(prompt,stdout);
       while (fgets(cmd,MAXCMDLENGTH,stdin)==NULL){
                fputs(prompt,stdout);
       }
       while (strchr(cmd,'\n')==NULL){
         tooLong=1;
         fgets(cmd,MAXCMDLENGTH,stdin);
       }
       if (tooLong){
	 printf("Exceeded command length\n");
         continue;
       }
       //
       //--- Commands are separated by pipe character
       //    Plan to spawn commands in reverse order (eliminates possibility of SIGPIPE)
       //    !!! Not dealing with input/output redirection
       //
       cmdCounter=0;
       while ((nextCmd=(char *)strrchr(cmd,'|'))!=NULL){
           *nextCmd='\0'; 
           nextCmd++;
	   aCmd[cmdCounter][0]=nextCmd;
           cmdCounter++;
       }
       aCmd[cmdCounter][0]=cmd; 
       cmdCounter++;
       for (i=0;i<cmdCounter;i++){
       	 printf("Command %d <%s>\n",i,aCmd[i][0]);
       }
       //
       //--- Separate each command into its arguments, 
       //    Remove whitespace along the way
       //    Note that we're ending the argument pointer array 
       //    with a NULL automagically
       //
       for (i=0;i<cmdCounter;i++){
	        printf("--------- Command %d ---------------------\n",i);
            aCmd[i][0]=(char *)strtok(aCmd[i][0]," \n");
	        printf("Arg 0 is <%s>. ",aCmd[i][0]);
            nargs=1;
            aCmd[i][nargs]=(char *)strtok(NULL," \n");
         
            while (aCmd[i][nargs]!=NULL){
	            printf("Arg %d is <%s>. ",nargs,aCmd[i][nargs]);
                nargs++;
                aCmd[i][nargs]=(char *)strtok(NULL," \n");
            }
            printf("\n\n");
       }

       //--- Note that we haven't dealt with input/output redirection.
       
  }
}
