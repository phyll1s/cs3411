/*
Andrew Markiewicz
CS3411
pgrm4 - tsh
*/

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>

#define MAXCMDLENGTH 120
#define MAXCMDS       20
#define MAXARGS       20

main(){
    char cmd[MAXCMDLENGTH];
    char prompt[]="tsh#";
    char *aCmd[MAXCMDS][MAXARGS];
    int  cmdCounter, i, nargs, tooLong;
    char *nextCmd;

    while (1==1){
        tooLong=0;
        
        //Wirte prompt
        if(fputs(prompt,stdout) == EOF){
            printf("Error fputs() - writing prompt\n");
            _exit(EXIT_FAILURE);        
        }
        
        //Get line of commands
        while(fgets(cmd,MAXCMDLENGTH,stdin)==NULL){
            if(!feof(stdin)){
                printf("Error fgets() - reading command\n");
                _exit(EXIT_FAILURE);     
            }
            if(fputs(prompt,stdout) == EOF){
                printf("Error fputs() - writing prompt\n");
                _exit(EXIT_FAILURE);        
            }
        }
        
        //If command entered is too long, continue reading until all input is read
        while(strchr(cmd,'\n')==NULL){
            tooLong=1;
            if(fgets(cmd,MAXCMDLENGTH,stdin) == NULL && !feof(stdin)){
                printf("Error fgets() - reading command\n");
                _exit(EXIT_FAILURE);
            }
        }
        
        //Remove newline read in by fgets
        if(cmd[strlen(cmd)-1] == '\n')
            cmd[strlen(cmd)-1] = '\0';
            
        if (tooLong){
            printf("Exceeded command length\n");
            continue;        
        }
        
        //
        //--- Commands are separated by pipe character
        //    Plan to spawn commands in reverse order (eliminates possibility of SIGPIPE)
        //    !!! Not dealing with input/output redirection
        //
        cmdCounter=0;
        while((nextCmd=(char *)strrchr(cmd,'|'))!=NULL){
            *nextCmd='\0';
            nextCmd++;
            aCmd[cmdCounter][0]=nextCmd;
            cmdCounter++;
        }
        aCmd[cmdCounter][0]=cmd;
        cmdCounter++;
        
       //
       //--- Separate each command into its arguments, 
       //    Remove whitespace along the way
       //    Note that we're ending the argument pointer array 
       //    with a NULL automagically
       //
       for (i=0;i<cmdCounter;i++){
            aCmd[i][0]=(char *)strtok(aCmd[i][0]," \n");
            nargs=1;
            aCmd[i][nargs]=(char *)strtok(NULL," \n");
         
            while(aCmd[i][nargs]!=NULL){
                nargs++;
                aCmd[i][nargs]=(char *)strtok(NULL," \n");
            }
            printf("\n");
        }
        
        //Check for internal commands
        if(!strcmp(aCmd[0][0],"q"))
            _exit(EXIT_SUCCESS);
        
        if(!strcmp(aCmd[0][0],"history")){
            printf("print history\n");
            continue;
        }
        
        if(!strcmp(aCmd[0][0],"prompt")){
            printf("Change term\n");
            continue;            
        }
            
        //Execute each command in order piping to the appropriate places
        for(i=0;i<cmdCounter;i++){
            int temp;
            
            temp = fork();
            if(temp == -1){
                printf("Error fork() - error forking new command\n");
                _exit(EXIT_FAILURE);             
            }
            
            //Child's code
            else if(temp == 0){
                if(execvp(aCmd[i][0],&(aCmd[i][0])) == -1){
                    printf("Error execvp() from process %i - %s\n",getpid(),strerror(errno));
                    break;
                }
            }
            
            //Parent waits for child
            else{
                wait(temp);
            }        
        }
        
    }
}
