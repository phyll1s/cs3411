#ifndef STACK_H
#define STACK_H
    //Define the Stack struct
    typedef struct{
        int capacity,top;
        char** elements;
    } Stack;
    
    //Stack modification
    int push(Stack*,char*);
    char* pop(Stack*);
    
    //Stack access
    char* top(Stack*);
    
    //Stack status
    int isEmpty(Stack*);


#endif
