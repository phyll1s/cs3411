/*
Andrew Markiewicz
Assignment 2 - CS3411
1/31/2011
Extract function
*/

#include <stdio.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>

int extract(int fd, void *buf, int bytes, int offset){

    /*Check for negative offset*/
    if(offset<0){
        printf("Error - cannot insert at a negative offset.\n");
        return -1;
    }
    
    /*Check for deletes beyond end of file*/
    struct stat fileStat;
    if(fstat(fd,&fileStat) == -1){
        printf("Error - fstat(): %i\n",strerror(errno));
        return -2;
    }
    if(offset + bytes > fileStat.st_size){
        printf("Error - cannot delete past end of file.\n");
        return -3;
    }
    
    if(fsync(fd) == -1){
        printf("Error - fsync():  %i\n",strerror(errno));
        return -4;
    }

    /*Backup from end of delete portion to end of file*/
    int begin,backupBytes;
    char* backup;
    begin = offset + bytes;
    backupBytes = lseek(fd,0,SEEK_END) - begin;
    backup = malloc(backupBytes);
    
    //Backup saved portion
    if(lseek(fd,begin,SEEK_SET) == -1){
        printf("Error - lseek():  %i\n",strerror(errno));
        return -4;
    }
    bcopy(&fd,backup,backupBytes);
    
    //Backup extracted portion
    if(lseek(fd,offset,SEEK_SET) == -1){
        printf("Error - lseek():  %i\n",strerror(errno));
        return -4;
    }
    int toReturn;
    toReturn = read(fd,buf,bytes);
    if(toReturn == -1){
        printf("Error - read():  %i\n",strerror(errno));
        return -4;
    }
    
    
    /*Truncate file*/
    int newLength;
    newLength = fileStat.st_size - bytes;
    if(ftruncate(fd,newLength) == -1){
        printf("Error - ftruncate():  %i\n",strerror(errno));
        return -4;
    }
    
    free(backup);
    return toReturn;
}
