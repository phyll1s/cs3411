/*
Andrew Markiewicz
Assignment 2 - CS3411
1/31/2011
Insert function
*/

#include <stdio.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdlib.h>
#include <strings.h>

int insert(int fd, void* buf, int bytes, int offset){
    
    /*Check for negative offset*/
    if(offset<0){
        printf("Error - cannot insert at a negative offset.\n");
        return -1;
    }
    
    /*Check for writes beyond end of file*/
    struct stat fileStat;
    if(fstat(fd,&fileStat) == -1){
        printf("Error - fstat(): %i\n",strerror(errno));
        return -2;
    }
    if(offset + bytes > fileStat.st_size){
        printf("Error - cannot inswer past end of file.\n");
        return -3;
    }
    
    if(fsync(fd) == -1){
        printf("Error - fsync():  %i\n",strerror(errno));
        return -4;
    }
    
    /*Backup data*/
    int backupBytes;
    backupBytes = lseek(fd,offset,SEEK_END) - lseek(fd,offset,SEEK_SET);
    char* backup = malloc(backupBytes);
    
    if(lseek(fd,offset,SEEK_SET) == -1){
        printf("Error - lseek():  %i\n",strerror(errno));
        return -4;
    }
    bcopy(&fd,backup,backupBytes);
    
    /*Insert new data*/
    if(lseek(fd,offset,SEEK_SET) == -1){
        printf("Error - lseek():  %i\n",strerror(errno));
        return -4;
    }
    int toReturn;
    toReturn = copy(buf,fd,bytes);
    
    /*Restore backedup data*/
    lseek(fd,offset+bytes,SEEK_SET);
    bcopy(backup,&fd,backupBytes);
    
    free(backup);
    return toReturn;
}
