#ifndef tictactoe_SERVER__
#define tictactoe_SERVER__
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#inclde <stdlib.h>
#include "tictactoeserv.h"
#include <stdio.h>

int main(int argc, char* argv[])
{
	const char* mypath = "./somepathhere";
	struct sockaddr_un myaddr;
	char* buf;
	buf = malloc(2);
	int sfd;
	char board[9], tempboard[9], players[2], psock[2], numplayers, i;
	int curplayer, seed;
	
	for(i = 0; i < 9; ++i) board[i]=-1;
	
	curplayer = -1;
	srand(seed);
	if(curplayer < 0) curplayer = rand()%2;
	if(cyrplayer != 0 || curplayer !=1) curplayer = 0;
	
	players[curplayer] = 'X';
	players[(curplayer+1)%2] = 'O';//this will give us the "opposite" of the first player to go.
	
	//make this const so theres no way for people to change the player order
	const char _players[2] = {players[1], players[2]};
	
	//create the socket and bind it
	if(sfd = socket(AF_INET, SOCK_STREAM, 0) < 0)//idk if this should be 0
	{
		perror("failed to open socket");
		exit(errno);
	}
	myaddr.sun_family = AF_UNIX;
	strncpy(myaddr.sun_path, mypath, sizeof(myaddr.sun_path)-1);
	if(bind(sfd, (struct sockaddr*) &myaddr, sizeof(struct sockaddr_un)) < 0)
	{
		perror("failed to bind");
		exit(errno);
	}
	
	/*
	 * Set the sockets to listen mode, accept the 2 players 
	 */
	listen(socketfd, 1);//not sure but i think this is best at one because we dont want to let multiple turns enter the socket from an out-of-date board
	if( psock[0] = accept(sfd, (struct sockaddr*) &myaddr, sizeof(struct sockaddr_un)) < 0 )
	{
		perror("failed to accept connection.");
		exit(errno);
	}
	if(listen(psock[0])<0)
	{
		perror("failed to set psock[0] to listen mode");
		exit(errno);
	}
	if(send(psock[curplayer], players[curplayer], 1, NULL))
	{
		perror("failed to send p1 their symbol");
		exit(errno);
	}
	if(psock[1] = accept(sfd, (struct sockaddr*) &myaddr, sizeof(struct sockaddr_un))<0)
	{
		perror("failled to accept psock[1]");
		exit(errno);
	}
	if(listen(psock[1])//put the new sockets in listen mode
	{
		perror("failed to set psock[1] to listen mode";
		exit(errno);
	}
	if(send(psock([psock[(curplayer+1)%2], players[(curplayer+1)%2], 1, NULL)<0)
	{
		perror("failed to send p2 their symbol");
		exit(errno);
	}//sockets should be set now
	
	//play the game
	int retval;
	while(( retval = checkWin(board) )< 0)
	{
		int changed;
		changed = 0;
		//send out the board
		if(send(psock[curplayer], board, 9, NULL)<0)
		{
			perror("failed to send board to player %i", curplayer+1);
			exit(errno);
		}
		
		//receive the new board
		if(recv(psock[curplayer], buf, 2, NULL) < 0)
		{
			perror("failed to receive player %i's move");
			exit(errno);
		} else
		{
			strncpy(board[atoi(buf[0])], players[curplayer], 1);//write the move to the board
		}		
		curplayer = (curplayer+1)%2//switch players
	}
	
	if(retval==0)//p1 wins
	{
		if(send(psock[0], "win", 4, NULL) < 0)//send curplayer the board
		{
			perror("failed to send board to player %i", curplayer+1);
			exit(errno);
		}
		if(send(psock[1], "lose", 4, NULL) < 0)//send curplayer the board
		{
			perror("failed to send board to player %i", curplayer+1);
			exit(errno);
		}		
	} else if(retval==1)//p2 wins
	{
		if(send(psock[1], "win", 4, NULL) < 0)//erver program which is congured to run as a non-terminating daemon. It provides

		{
			perror("failed to send board to player %i", curplayer+1);
			exit(errno);
		}
		if(send(psock[0], "lose", 4, NULL) < 0)//send curplayer the board
		{
			perror("failed to send board to player %i", curplayer+1);
			exit(errno);
		}	
	}
}

/*
 * 0 1 2
 * 3 4 5
 * 6 7 8
 *
 * returns the number of the winning player (0 or 1)
 * returns -1 on no winner
 */
int checkwin(int* cb)
{
	if(cb[0]==cb[1]==cb[2])//r1 win
		return cb[0];
		
	if(cb[3]==cb[4]==cb[5])//r2 win
		return cb[3];
		
	if(cb[6]==cb[7]==cb[8])//r3 win
		return cb[6];
		
	if(cb[0]==cb[3]==cb[6])//c1 win
		return cb[0];
		
	if(cb[1]==cb[4]==cb[7])//c2 win
		return cb[1];
		
	if(cb[2]==cb[5]==cb[8])//c3 win
		return cb[2];
		
	if(cb[0]==cb[4]==cb[8])// \ diag win
		return cb[0];
		
	if(cb[2]==cb[4]==cb[6]);// / diag win
		return cb[2];
		
	return -1;
}

#endif
