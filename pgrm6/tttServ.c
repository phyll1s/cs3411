#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <signal.h>
#include <sys/wait.h>

#include "tttServ.h"

/*Game status:
0 - no one in the game
1 - 1 person waiting for an opponent
2 - 2 people are playing a game
*/
int gameStatus;
char* handles[2];

main(int args, char* argv[]){
    int c1sockFD,c2sockFD,cAsockFD,qSockFD;
    int handleLen;
    handleLen = 80;
    gameStatus = 0;
    handles[0] = malloc(handleLen+1);
    handles[1] = malloc(handleLen+1);
    
    //Check for no input
    if(args != 1){
        exitFailure("Error wrong input - there should be no arguments");   
    }
    
    //Create client and query sockets
    c1sockFD = socket(AF_INET,SOCK_STREAM,0);
    c2sockFD = socket(AF_INET,SOCK_STREAM,0);
    cAsockFD = socket(AF_INET,SOCK_STREAM,0);
    qSockFD = socket(AF_INET,SOCK_DGRAM,0);
    
    if(c1sockFD == -1 || c2sockFD == -1 || cAsockFD == -1 || qSockFD == -1){
        exitFailure("socket()");
    }
    
	//Bind the 1st socket
	int addrLen;
    struct sockaddr_in sockAddrIn1;
	sockAddrIn1.sin_family = AF_INET;
	sockAddrIn1.sin_addr.s_addr = htonl(INADDR_ANY);
	sockAddrIn1.sin_port = htons(0);
    addrLen = sizeof(sockAddrIn1);
    
	if(bind(c1sockFD,(struct sockaddr *)&sockAddrIn1,addrLen) != 0){
        exitFailure("bind()1");
	}
	if(getsockname(c1sockFD,(struct sockaddr *)&sockAddrIn1,&addrLen) == -1){
        exitFailure("getsockname()");
	}

	
	//Bind the 2nd socket
    struct sockaddr_in sockAddrIn2;
	sockAddrIn2.sin_family = AF_INET;
	sockAddrIn2.sin_addr.s_addr = htonl(INADDR_ANY);
	sockAddrIn2.sin_port = htons(0);
    addrLen = sizeof(sockAddrIn2);
    
	if(bind(c2sockFD,(struct sockaddr *)&sockAddrIn2,addrLen) != 0){
		exitFailure("bind()2");
	}
	if(getsockname(c2sockFD,(struct sockaddr *)&sockAddrIn2,&addrLen) == -1){
		exitFailure("getsockname()");
	}    
	
	//Bind the 3rd socket
    struct sockaddr_in sockAddrIn3;
	sockAddrIn3.sin_family = AF_INET;
	sockAddrIn3.sin_addr.s_addr = htonl(INADDR_ANY);
	sockAddrIn3.sin_port = htons(0);
    addrLen = sizeof(sockAddrIn3);
    
	if(bind(cAsockFD,(struct sockaddr *)&sockAddrIn3,addrLen) != 0){
		exitFailure("bind()3");
	}
	if(getsockname(cAsockFD,(struct sockaddr *)&sockAddrIn3,&addrLen) == -1){
		exitFailure("getsockname()");
	}
	
	//Bind the query socket
    struct sockaddr_in sockAddrIn4;
	sockAddrIn4.sin_family = AF_INET;
	sockAddrIn4.sin_addr.s_addr = htonl(INADDR_ANY);
	sockAddrIn4.sin_port = htons(0);
    addrLen = sizeof(sockAddrIn4);
    
	if(bind(qSockFD,(struct sockaddr *)&sockAddrIn4,addrLen) < 0){
		exitFailure("bind()4");
	}
	if(getsockname(qSockFD,(struct sockaddr *)&sockAddrIn4,&addrLen) == -1){
		exitFailure("getsockname()");
	}
	
	//Write hostname and socket ports to config
    FILE * configFile;
    configFile = fopen("tTt.cfg","w+");
    if(configFile == NULL){
        exitFailure("fopen()");
    }
    
    int hostNameLen;
    hostNameLen = 256;
    char hostName[hostNameLen];
    if(gethostname(hostName,hostNameLen) == -1){
        exitFailure("gethostname()");
    }
    
	if(fprintf(configFile,"%s\n%d\n%d\n%d\n%d\n",hostName,sockAddrIn1.sin_port,
	    sockAddrIn2.sin_port,sockAddrIn3.sin_port,sockAddrIn4.sin_port) < 0){
        exitFailure("fprintf()");
	}		
	//Close the config file
	if(fflush(configFile) != 0){
	    exitFailure("fflush()");
	}
	if(fclose(configFile) != 0){
        exitFailure("fclose()");
	}

	//Fork
	int childPid;
	childPid = fork();
	if(childPid == -1){
		exitFailure("fork()");
	}
	
	//Child handles queries
	else if(childPid == 0){
	    printf("Server ready to handle queries\n");
	    int buffLen;
	    buffLen = 6;
	    char buff[buffLen];
	    
	    while(1){
	        if(recv(qSockFD,&buff,buffLen,0) == -1){
	            exitFailure("recv()");
	        }
	        if(!strcmp(buff,"query")){
	            char* toSend;
	            int i;
	            toSend = malloc(sizeof(gameStatus)+1);
	            sprintf(toSend,"%i",gameStatus);
	            if(send(qSockFD,toSend,strlen(toSend),0) == -1){
	                printf("Error sending query\n");
	            }
	            free(toSend);
	            for(i=0;i<gameStatus;i++){
	                toSend = malloc(sizeof(handles[i])+1);
	                sprintf(toSend,"%s",handles[i]);
	                if(send(qSockFD,toSend,strlen(toSend),0) == -1){
	                    printf("Error sending query\n");
	                }
	                free(toSend);
                }	            
	        }
	    }
	}
	
	//Parent handles gameplay
	else{
	    printf("Server ready to handle players\n");
        //while(1){
            
        //}
    }    
    //Send term to child and wait
    int i;
    for(i=0;i<5;i++){
        printf("Attempting to kill child process - try %i of 4\n",i);
        if(kill(childPid,SIGTERM) == 0){
            printf("Child killed\n");
            break;
        }
    }
    wait(NULL);
}

void exitFailure(char* msg){
    printf("Error %s - %s\n",msg,strerror(errno));
    if(handles[0]!=NULL){free(handles[0]);}
    if(handles[1]!=NULL){free(handles[1]);}
    exit(EXIT_FAILURE);
}
