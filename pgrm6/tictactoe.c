#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

int main(int argc, char* argv[])
{
	unsigned int seed;
	char symbol, cmd[2];
	char* buf;
	buf = malloc(80);//we should never read more than this
	curPlyr = winner = -1;
	int i, sfd;
	char* mypath = "./somepath"//idk what to put here
	struct sockaddr myaddr;
	
	//create and bind the socket
	if(sfd = socket(AF_INET,SOCK_STREAM,0) < 0){
		perror("failed to create the socket");
		exit(errno);
	}
	myaddr.sun_family = AF_UNIX;
	strncpy(myaddr.sun_path, mypath, sizeof(myaddr.sun_path)-1);
	if(bind(sfd, (struct sockaddr*) &myaddr, sizeof(struct sockaddr_un)) < 0)
	{
		perror("failed to bind");
		exit(errno);
	}
	connect(sfd, myaddr, strlen(mypath)-1);
	
	//get win status
	if(recv(sfd, buf, 5, 0) < 0)
	{
		perror("failed to receive win status");
	}
	
	//tell the player they win if they did
	if(strcmp(buf, "win") == 0)
	{
		fprintf("You win!");
		exit(0);
	} else if(strcmp(buf, "lose") == 0)
	{
		fprintf("You lose!");
		exit(0);
	}
	
	if(recv(sfd, buf, 10, 0) < 0(
	{
		perror("failed to receive board");
		exit(errno);
	}
	printboard(buf);
	//get input should only need 2 chars, (input)\n
	getin:
	while(!fgets(cmd, 2, stdin))
	{
		char ch;
		fputs("It is your turn. Please select a square.", stdout);
	}
	int b;
	b = 0;
	for(i=0; i<9; ++i)
		if(cmd[0]==buf[i])//check if the input char is the correct form
		{
			b = 1;
			break;
		}
	if(b!=1)
	{
		fprintf(stdout, "Invalid input: %c", cmd[0]);
		goto getin;
	} else
	{
		if(send(sfd, cmd, 2, 0)<0)//send out cmd
		{
			perror("failed to send move");
			exit(errno);
		}
	}
}

/* print the board */
void printboard(char* cb)
{
	fprintf("//-----\\\\\n||%c|%c|%c||\n||%c|%c|%c||\n||%c|%c%c||\n\\\\-----//\n", cb[0], cb[1], cb[2], cb[3], cb[4], cb[5], cb[6], cb[7], cb[8], cb[9]);
}
