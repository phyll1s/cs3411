#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include "tttClient.h"

main(int args, char* argv[]){
    char* hostName;
    int port1,port2,portA,portQ;
    int queryMode,timeoutMode;
    queryMode = 0;timeoutMode = 0;
    
    //Read config file
    hostName = NULL;
    if(readConfig(&hostName,&port1,&port2,&portA,&portQ) == -1){
        free(hostName);
        exitFailure("no config file");    
    }
    printf("Hostname:%s\nport1:%i\nport2:%i\nportA:%i\nportQ:%i\n",hostName,port1,port2,portA,portQ);
    
    //Check for mode input
    if(args == 2){
        //Query Mode
        if(!strcmp(argv[1],"-q")){
            queryMode = 1;
            
        //Timeout mode
        }else if(!strcmp(argv[1],"-t")){
            timeoutMode = 1;

        }else{
            exitFailure("Incorrect arguments\n");          
        }
    }else if(args > 2){
        exitFailure("Incorrect arguments\n"); 
    }
    
    //Get handle from user
    int handleLen;    
    char *handle;
    handleLen = 80;
    handle = malloc(handleLen+1);
    printf("Please input a handle (80 chars max):");
    if(fgets(handle,handleLen+1,stdin) == NULL){
        free(handle);
        exitFailure("Error must input a handle");
    }
    handle[strlen(handle)-1] = '\0';
    
    //Find host info
    struct sockaddr_in sockAddrIn;
    struct hostent *serverInfo;
    serverInfo = gethostbyname(hostName);
	if(serverInfo == NULL){
		free(hostName);
		exitFailure("gethostbyname()");
	}
	
	//Set server info
    char * servIp;
    int addrLen;
    struct in_addr servFormattedIp;
       
    //FIGURE THIS SHIT OUT//
    
    
    serv = gethostbyname(hostName);
    servIp = inet_ntoa((struct in_addr *)*serv->h_addr_list);    
    inet_aton(servIp,&servFormattedIp);

	sockAddrIn.sin_family = AF_INET;
	sockAddrIn.sin_addr.s_addr = servFormattedIp.s_addr;
	sockAddrIn.sin_port = portQ;
    addrLen = sizeof(sockAddrIn);
    
    //Query server to check status
    int qSockFD;
    qSockFD = socket(AF_INET,SOCK_DGRAM,0);
    if(qSockFD == -1){
        free(hostName);
        exitFailure("socket()");
    }
    connect(qSockFD,((struct sockaddr)sockAddrIn),addrLen);
    
    
    //Connect if there's players
    
    
    
    //Cleanup and exit
    free(handle);
    free(hostName);
}

int queryServer(char* p1, char* p2){

}

int readConfig(char** host, int* p1, int* p2, int* pA, int* pQ){
    //Read info from config file
    //Setup file
    FILE * configFile;
    configFile = fopen("tTt.cfg","r");
    if(configFile == NULL){
        printf("Error opening config file, trying again in 60s\n");
        wait(60);
        configFile = fopen("tTt.cfg","r");
        if(configFile == NULL){
            return -1;
        }
    }
    
    //Read hostname
    size_t temp;    
    temp = 256;
    if(getline(host,&temp,configFile) == -1){
        return -1;
    }
    host[0][strlen(*host)-1] = '\0';
    
    //Read port 1
    char* tempInput;
    tempInput = NULL;
    if(getline(&tempInput,&temp,configFile) == -1){
        if(tempInput != NULL){free(tempInput);}
        return -1;
    }
    *p1 = atoi(tempInput);
    free(tempInput);
    
    //Read port 2
    tempInput = NULL;
    if(getline(&tempInput,&temp,configFile) == -1){
        if(tempInput != NULL){free(tempInput);}
        return -1;
    }
    *p2 = atoi(tempInput);
    free(tempInput);
    
    //Read port A
    tempInput = NULL;
    if(getline(&tempInput,&temp,configFile) == -1){
        if(tempInput != NULL){free(tempInput);}
        return -1;
    }
    *pA = atoi(tempInput);
    free(tempInput);
    
    //Read port Q
    tempInput = NULL;
    if(getline(&tempInput,&temp,configFile) == -1){
        if(tempInput != NULL){free(tempInput);}
        return -1;
    }
    *pQ = atoi(tempInput);
    free(tempInput);
    
    if(fclose(configFile) != 0)
        return -1;
    else
        return 0;
}

void exitFailure(char* msg){
    printf("Error %s - %s\n",msg,strerror(errno));
    exit(EXIT_FAILURE);
}
