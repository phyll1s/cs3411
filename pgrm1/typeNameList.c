#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>

#include "typeNameList.h"
/*==================================================================
  char **typeNameList(char *dirName, int fileType)

  Creates a list of names for all files of type fileType in the 
  directory tree rooted at dirName.  List is an array of character
  pointers.  Returns pointer to first element of the array. Last member
  is NULL to indicate the end of the list.

  All memory allocation is done within this subroutine.  Caller is
  responsible to delete the allocated memory.  

  Expects that dirName is the fully-qualified name of a directory.
 ==================================================================
*/

int count = 0;
int sizeSum = 0;
char* *fileList = NULL;
    
char **typeNameList(char *dirName, unsigned fileType){

    if(fileType < 1 || fileType > 7){
        printf("Invalid fileType\n");
        return NULL;
    }    
    
    DIR     *dirPtr;        /* Pointer to directory stream for dirName */
    struct  dirent *entry;
    struct  dirent *result;      /* Pointer to return from readdir_r */
    struct  stat entryStats;     /* File info given by lstat */
    char    *fullName;           /* Fully qualified file name */
    int     retCode;             /* Return code */

    //Open directory, return NULL if error doing so
    dirPtr=opendir(dirName);
    if(dirPtr == NULL){
        printf("Error opening directory.\n%s\n",strerror(errno));
        return NULL;
    }
    
    size_t length;
    length = offsetof(struct dirent, d_name ) + pathconf(dirName, _PC_NAME_MAX) + 1;
    entry = malloc(length);
    
    retCode=readdir_r(dirPtr,entry,&result);

    //Using the same code from traverseDir.c as it does the needed actions
    while((retCode==0) && (result!=NULL)){

        /* --- Create fully qualified file namechar* addToArray(char* toAdd); 	
        --- Return from readdir_r is relative to dirName
        */
        size_t tempSize;
        tempSize = strlen(dirName) + 2 + strlen(entry->d_name);
        fullName = malloc(tempSize);
        
        fullName[0] = '\0';
        strcat(fullName,dirName);
        strcat(fullName,"/");
        strcat(fullName,entry->d_name);

        /* Get file information*/
        /*Use lstat to make sure we get info on symbolic link, not target*/    
        lstat(fullName,&entryStats);
        
        /* Regular file */
        if(S_ISREG(entryStats.st_mode) && fileType == 1){            
            if(addToArray(fullName) == -1)
                return NULL;
        }

        /*  Directory */
        else if(S_ISDIR(entryStats.st_mode)){
            if(strcmp(".",entry->d_name) && strcmp("..",entry->d_name)){
                if(fileType == 2){
                    if(addToArray(fullName) == -1)
                        return NULL;
                }
                typeNameList(fullName,fileType);
            }
        }
        
        /* Character special */
        else if(S_ISCHR(entryStats.st_mode) && fileType == 3){     
            if(addToArray(fullName) == -1)
                return NULL;
        }
        
        /* Block special */    
        else if(S_ISBLK(entryStats.st_mode) && fileType == 4){
            if(addToArray(fullName) == -1)
                return NULL;
        }
        
        /* FIFO */
        else if(S_ISFIFO(entryStats.st_mode) && fileType == 6){ 
            if(addToArray(fullName) == -1)
                return NULL;   
        }
        
        /* Symbolic link */     
        else if (S_ISLNK(entryStats.st_mode) && fileType == 5){    
            if(addToArray(fullName) == -1)
                return NULL;         
        } 
        
        /* UNIX domain socket */
        else if (S_ISSOCK(entryStats.st_mode) && fileType == 7){
            if(addToArray(fullName) == -1)
                return NULL;    
        }
        
        else{
            free(fullName);     
        }
        retCode=readdir_r(dirPtr,entry,&result);
    }

    free(entry);
    closedir(dirPtr);

    return fileList;
}

//Method to dynamically add elements to the array
int addToArray(char* toAdd){
    sizeSum = sizeSum + sizeof(toAdd) + 1 + sizeof(char*);
    char* *tempArray = realloc(fileList, sizeSum);
    
    if (tempArray == NULL){
        printf("Error allocating memory\n");
        return -1;
    }
    fileList = (char**)tempArray;
    
    fileList[count] = toAdd;
    fileList[(count+1)] = NULL;
    count++;  
    return 0;
}
