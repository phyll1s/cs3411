#include <stdlib.h>
#include <stdio.h>

#include "traverseDir.h"

int main(int argc, char *argv[]){
  
  if(argc == 2){
      traverseDir(argv[1]);
  }else{
      printf("Incorrect input - exiting\n");
      exit(1);
  }
}
