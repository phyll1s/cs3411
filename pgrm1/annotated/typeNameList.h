#ifndef TYPENAMELIST_H
#define TYPENAMELIST_H

    #define TNL_REGULAR  1
    #define TNL_DIR      2
    #define TNL_CHARSPCL 3
    #define TNL_BLKSPCL  4
    #define TNL_SYMLINK  5
    #define TNL_FIFO     6
    #define TNL_UNIXSOCK 7

    char **typeNameList(char *dirName, unsigned fileType);
    int addToArray(char* toAdd);
    
#endif

