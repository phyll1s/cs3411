#include <stdlib.h>
#include <stdio.h>

#include "traverseDir.h"

int main(int argc, char *argv[]){
  
  //Added checking to make sure that the 2 needed arguments are passed in
  if(argc == 2){
      traverseDir(argv[1]);
  }else{
      printf("Incorrect input - exiting\n");
      exit(1);
  }
}
