#include <dirent.h>
#include <sys/types.h>
#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>

#include "traverseDir.h"
/*==================================================================
  void traverseDir(char *dirName)

  Prints out the name of each file in the directory tree
  rooted at dirName.  Also prints the file type.
 ==================================================================
*/
void traverseDir(char *dirName){
    
    //Removed unused variables: i,ret2,entrystats2
    //Changed entry to a pointer
    DIR     *dirPtr;        /* Pointer to directory stream for dirName */
    struct  dirent *entry;
    struct  dirent *result;      /* Pointer to return from readdir_r */
    struct  stat entryStats;     /* File info given by lstat */
    char    *fullName;           /* Fully qualified file name */
    int     retCode;             /* Return code */
    
    //Removed malloc for fullname
    //Added error checking for opening folder
    dirPtr=opendir(dirName);
    if(dirPtr == NULL){
        printf("Error opening directory.\n%s\n",strerror(errno));
        exit(1);
    }
    
    //Allocated proper space for entry based on max amount of data needed to be stored
    size_t length;
    length = offsetof(struct dirent, d_name ) + pathconf(dirName, _PC_NAME_MAX) + 1;
    entry = malloc(length);
    
    retCode=readdir_r(dirPtr,entry,&result);
    
    while((retCode==0) && (result!=NULL)){

        /* --- Create fully qualified file name 
        --- Return from readdir_r is relative to dirName
        */
        
        //Here malloc the appropriate amount of memory for entry
        size_t tempSize;
        tempSize = strlen(dirName) + 2 + strlen(entry->d_name);
        fullName = malloc(tempSize);
        
        //Empty string before concat'ing more to it for obv reasons
        fullName[0] = '\0';
        strcat(fullName,dirName);
        strcat(fullName,"/");
        strcat(fullName,entry->d_name);

        /* Get file information*/
        /*Use lstat to make sure we get info on symbolic link, not target*/    
        lstat(fullName,&entryStats);
        
        /******************************************************/
        /*APPLIES FOR FOLLOWING IF,ELSE-IF,ELSE LOOP STRUCTURE*/
        /*Removed puts as it is an unsafe opperation          */
        /*Combined multiple printf statements into one        */
        /*Fixed formatting/indentation to follow standards    */
        /******************************************************/
        
        /* Regular file */
        if(S_ISREG(entryStats.st_mode)){           
            printf("<%s> is a regular file\n",fullName);
        }
        
        /*  Directory */
        else if(S_ISDIR(entryStats.st_mode)){
            /*--- "." is a reference to the current directory  
            --- ".." is a reference to the parent directory
            --- Don't follow these, they are redundant to our
            --- traversal path
            --- strcmp returns 0 on an exact match
            */ 	
            if(strcmp(".",entry->d_name) && strcmp("..",entry->d_name)){
                printf("<%s>  is a directory\n",fullName);
                traverseDir(fullName);
            }
        }
        
        /* Character special */
        else if(S_ISCHR(entryStats.st_mode)){     
            printf("<%s> is a character device\n",fullName);
        }
        
        /* Block special */    
        else if(S_ISBLK(entryStats.st_mode)){
            printf("<%s> is a block device\n",fullName);
        }
        
        /* FIFO */
        else if(S_ISFIFO(entryStats.st_mode)){    
            printf("<%s> is a fifo\n",fullName);
        }
        
        /* Symbolic link */     
        else if (S_ISLNK(entryStats.st_mode)){     
            printf("<%s> is a symbolic link\n",fullName);        
        } 
        
        /* UNIX domain socket */
        else if (S_ISSOCK(entryStats.st_mode)){    
            printf("<%s> is a socket\n",fullName);        
        }
        
        /* Something's very wrong */
        else{                             
            printf("<%s>  has an unrecognized file type\n",fullName);     
        }
        
        //flust the stdout for good measure
        fflush(stdout);
        
        //Free fullName as the amount of memory will (usually) change for each
        //file
        free(fullName);     
        retCode=readdir_r(dirPtr,entry,&result);
    }
    
    //Free allocated memory for entry
    free(entry);
    closedir(dirPtr);
}
