#include <dirent.h>
#include <sys/types.h>
#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

int main(int argc, char *argv[]){

  int i;
  char **nameList;

  nameList=typeNameList(argv[1],atoi(argv[2]));
  //Check if there was an error when running typeNameList
  if(nameList == NULL){
    printf("Error while traversing folder\n");
    exit(1);
  }
  for (i=0;nameList[i]!=(char *)0;i++) {
      printf("%d:<%s>\n",i,nameList[i]);
      free(nameList[i]);
  }
  free(nameList);
}

