#include <dirent.h>
#include <sys/types.h>
#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>

#include "traverseDir.h"
/*==================================================================
  void traverseDir(char *dirName)

  Prints out the name of each file in the directory tree
  rooted at dirName.  Also prints the file type.
 ==================================================================
*/
void traverseDir(char *dirName){

    DIR     *dirPtr;        /* Pointer to directory stream for dirName */
    struct  dirent *entry;
    struct  dirent *result;      /* Pointer to return from readdir_r */
    struct  stat entryStats;     /* File info given by lstat */
    char    *fullName;           /* Fully qualified file name */
    int     retCode;             /* Return code */

    dirPtr=opendir(dirName);
    if(dirPtr == NULL){
        printf("Error opening directory.\n%s\n",strerror(errno));
        exit(1);
    }
    
    size_t length;
    length = offsetof(struct dirent, d_name ) + pathconf(dirName, _PC_NAME_MAX) + 1;
    entry = malloc(length);
    
    retCode=readdir_r(dirPtr,entry,&result);
    
    while((retCode==0) && (result!=NULL)){

        /* --- Create fully qualified file name 
        --- Return from readdir_r is relative to dirName
        */
        size_t tempSize;
        tempSize = strlen(dirName) + 2 + strlen(entry->d_name);
        fullName = malloc(tempSize);
        
        fullName[0] = '\0';
        strcat(fullName,dirName);
        strcat(fullName,"/");
        strcat(fullName,entry->d_name);

        /* Get file information*/
        /*Use lstat to make sure we get info on symbolic link, not target*/    
        lstat(fullName,&entryStats);
        
        /* Regular file */
        if(S_ISREG(entryStats.st_mode)){           
            printf("<%s> is a regular file\n",fullName);
        }
        
        /*  Directory */
        else if(S_ISDIR(entryStats.st_mode)){
            /*--- "." is a reference to the current directory  
            --- ".." is a reference to the parent directory
            --- Don't follow these, they are redundant to our
            --- traversal path
            --- strcmp returns 0 on an exact match
            */ 	
            if(strcmp(".",entry->d_name) && strcmp("..",entry->d_name)){
                printf("<%s>  is a directory\n",fullName);
                traverseDir(fullName);
            }
        }
        
        /* Character special */
        else if(S_ISCHR(entryStats.st_mode)){     
            printf("<%s> is a character device\n",fullName);
        }
        
        /* Block special */    
        else if(S_ISBLK(entryStats.st_mode)){
            printf("<%s> is a block device\n",fullName);
        }
        
        /* FIFO */
        else if(S_ISFIFO(entryStats.st_mode)){    
            printf("<%s> is a fifo\n",fullName);
        }
        
        /* Symbolic link */     
        else if (S_ISLNK(entryStats.st_mode)){     
            printf("<%s> is a symbolic link\n",fullName);        
        } 
        
        /* UNIX domain socket */
        else if (S_ISSOCK(entryStats.st_mode)){    
            printf("<%s> is a socket\n",fullName);        
        }
        
        /* Something's very wrong */
        else{                             
            printf("<%s>  has an unrecognized file type\n",fullName);     
        }
        
        fflush(stdout);
        free(fullName);     
        retCode=readdir_r(dirPtr,entry,&result);
    }
    
    free(entry);
    closedir(dirPtr);
}
