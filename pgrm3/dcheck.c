#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <errno.h>
#include <sys/stat.h>

int main(int argc, char *argv[]){
    int nprocs,sep;
    
    //Check for sep argument & proper # of arguments
    if(argc == 4){
        sep = 10;
    }else if(argc == 5){
        sep = atoi(argv[4]);
    }else{
        printf("Error - wrong number of arguments\n");
        exit(EXIT_FAILURE);
    }
    nprocs = atoi(argv[3]);
    
    //Get filesize
    struct stat *fileStats = malloc (sizeof *fileStats);
    if(stat(argv[2],fileStats) == -1){
        printf("Error stat() while getting filesize - %i\n",strerror(errno));
        free(fileStats);
        exit(EXIT_FAILURE);
    }

    //Loop & spawn procs
    int i,currSize,sizePerProc;
    int cids[nprocs];
    currSize = 0;
    sizePerProc = (fileStats->st_size)/nprocs;
    for(i=0;i<nprocs;i++){
        char start[50];
        char end[50];
        char sepC[50];
        sprintf(start,"%d",currSize);
        currSize = currSize + sizePerProc;
        sprintf(end,"%d",currSize);
        sprintf(sepC,"LINETERM=%i",sep);

        //Format exec arguments
        char* newArgs[] = {argv[1],argv[2],start,end,'\0'};
        char* envp[] = {sepC,'\0'};
        
        int cid;
        cid = fork();
        //Error forking
        if(cid == -1){
            printf("Error forking - %i\n",strerror(errno));
            free(fileStats);
            exit(EXIT_FAILURE);           
        }
        //child
        else if(cid == 0){
            if(execve("./bot",newArgs,envp) == -1){
                printf("Error execve() from process %i - %i\n",getpid(),strerror(errno));
                free(fileStats);
                exit(EXIT_FAILURE);
            }
        }
        //Parent
        else{
            cids[i] = cid;
        }
    }
    
    //Clean up procs
    for(i=0;i<nprocs;i++){
        int exitStatus;
        waitpid(cids[i],&exitStatus,0);
        if(WIFEXITED(exitStatus)){
            printf("Child %d exited with the exit status of \"%d\"\n",cids[i],WEXITSTATUS(exitStatus));
            //If the word is fouund by a child
            if(WEXITSTATUS(exitStatus) == 0){
                printf("-->Child %d found the word!<--\n\n",cids[i]);
                printf("Remaining children signaled to end\n");
                int j;
                for(j=0;j<nprocs;j++){
                    if(j != i)
                        kill(cids[j],SIGTERM);
                }
                free(fileStats);
                exit(EXIT_SUCCESS);
                
            //Child term'd with a + exit status
            }else if(WEXITSTATUS(exitStatus) > 0){
                printf("Child %d exited with an exit status greater than 1: %i\n",cids[i],WEXITSTATUS(exitStatus));
            }
        }
    }
    //Reaches here if the word is never found
    printf("\n-->The word \'%s\' was not found in the dictionary<--\n",argv[1]);
    free(fileStats);
}
