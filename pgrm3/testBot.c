#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#define _GNU_SOURCE

int main(int argc, char *argv[]){
    //Setup variables
    char* word,*file;
    int start,end;

    word = argv[0];
    file = argv[1];
    start = atoi(argv[2]);
    end = atoi(argv[3]);    
    
    char sep;
    sep = atoi(getenv("LINETERM"));

    //Open file
    FILE *dic;
    dic = fopen(file,"r");
    if(dic == NULL){
        printf("Error fopen() from process %i - %s\n",getpid(),strerror(errno));
        exit(-1);
    }
    
    //"seek" to the begining of your section
    int currPos;
    size_t n;
    n = 0;
    currPos = 0;
    while(currPos<start){
        char* tempRead;
        int read;
        read = getdelim(&tempRead,&n,sep,dic);
        if(read == -1 && !feof(dic)){
            printf("E46rror getdelim() from process %i - %s\n",getpid(),strerror(errno));
            exit(-1); 
        }
        currPos+=read;        
    }
    
    //Read in your section of the file
    while(currPos<end){
        char* token;
        int read;
        read = getdelim(&token,&n,sep,dic);
        
        //If error reading & not eof
        if(read == -1 && !feof(dic)){
            printf("E61rror getdelim() from process %i - %s\n",getpid(),strerror(errno));
            exit(-1); 
        }else if(!feof(dic)){
            token[strlen(token)-1] = '\0';
        }
        printf("[%i] - <%s>\n",getpid(),token);
        //Exit if word is found
        if(!strcmp(token,word)){
            fflush(stdout);
            exit(0);      
        }
        
        currPos+=read;
    }
    
    fflush(stdout);
    exit(2);
}
