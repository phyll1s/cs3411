#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

main(int argc, char *argv[]){
    //Check # of arguments
    if(argc != 2){
        printf("Error - incorrect number of arguments\n");
        printf("Useage; pingOne host interval\n");
        exit(EXIT_FAILURE);    
    }
    
    sleep(atoi(argv[1]));

    int t1,t2;
    t1 = open("/dev/null",O_WRONLY);
    t2 = open("/dev/null",O_WRONLY);
    if(t1 <0 || t2 <0)
        _exit(3);
        
    dup2(t1,STDOUT_FILENO);
    dup2(t2,STDIN_FILENO);
    
    if(execlp("/bin/ping","/bin/ping","-c","1","-W","2",argv[0],(char*)0) == -1){
        printf("Error close() - %i\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    _exit(3);
}
