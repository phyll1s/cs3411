#include "sonar.h"

#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <errno.h>

char* hosts[10];
char* hostFile;
char* deadHosts;
int interval,numHosts,groupId;

main(int argc, char *argv[]){

    //Check for correct # of parameters
    if(argc != 4){
        printf("Error - incorrect amount of parameters\n");
        printf("Useage: sonar hostFile deadHosts interval\n");
        exit(EXIT_FAILURE);
    }
    hostFile = argv[1];
    deadHosts = argv[2];
    interval = atoi(argv[3]);
    
    //Block SIGALRM, SIGHUP, SIGTERM
    sigset_t toBlock,oldSet;
    if(sigemptyset(&toBlock) == -1){
        printf("Error sigemptyset()\n");
        exit(EXIT_FAILURE);
    }
    if(sigaddset(&toBlock,SIGALRM) == -1){
        printf("Error sigaddset()\n");
        exit(EXIT_FAILURE);
    }    
    if(sigaddset(&toBlock,SIGHUP) == -1){
        printf("Error sigaddset()\n");
        exit(EXIT_FAILURE);
    }
    if(sigaddset(&toBlock,SIGTERM) == -1){
        printf("Error sigaddset()\n");
        exit(EXIT_FAILURE);
    }
    if(sigprocmask(SIG_BLOCK,&toBlock,&oldSet) == -1){
        printf("Error sigprocmask()\n");
        exit(EXIT_FAILURE);
    }
    
    
    //Read hostFile     
    //->Initialize list of hosts
    int i;
    for(i=0;i<10;i++){
        hosts[i] = malloc(81);
    }
    getHosts(hostFile,hosts);
    
    //Install handlers for SIGALRM, SIGHUP, SIGTERM
    struct sigaction  act;
    
    //SIGHUP
    act.sa_handler = getHosts;
    act.sa_flags  = 0;
    if(sigemptyset(&act.sa_mask) == -1){printf("Error sigemptyset()\n");exit(EXIT_FAILURE);}
    if(sigaddset(&act.sa_mask,SIGTERM)){printf("Error sigaddset()\n");exit(EXIT_FAILURE);}
    if(sigaddset(&act.sa_mask,SIGALRM)){printf("Error sigaddset()\n");exit(EXIT_FAILURE);}
    if(sigaction(SIGHUP,&act,NULL)){printf("Error sigaction()\n");exit(EXIT_FAILURE);}
    
    //SIGALRM
    act.sa_handler = pingHosts;
    act.sa_flags  = SA_NODEFER;
    if(sigemptyset(&act.sa_mask)){printf("Error sigemptyset()\n");exit(EXIT_FAILURE);}
    if(sigaddset(&act.sa_mask,SIGHUP)){printf("Error sigaddset()\n");exit(EXIT_FAILURE);}
    if(sigaction(SIGALRM,&act,NULL)){printf("Error sigaction()\n");exit(EXIT_FAILURE);} 
    
    //SIGTERM
    act.sa_handler = cleanup;
    act.sa_flags  = 0;
    if(sigemptyset(&act.sa_mask)){printf("Error sigemptyset()\n");exit(EXIT_FAILURE);}
    if(sigaddset(&act.sa_mask,SIGHUP)){printf("Error sigaddset()\n");exit(EXIT_FAILURE);}
    if(sigaddset(&act.sa_mask,SIGALRM)){printf("Error sigaddset()\n");exit(EXIT_FAILURE);}
    if(sigaction(SIGTERM,&act,NULL)){printf("Error sigaction()\n");exit(EXIT_FAILURE);}     

    
    //Unblock SIGARLM, SIGHUP, SIGTERM
    if(sigprocmask(SIG_SETMASK,&oldSet,NULL) == -1){
        printf("Error sigprocmask()\n");
        exit(EXIT_FAILURE);
    }    
    
    //Set alarm
    alarm(interval); 
    
    //Write PID to STDOUT
    printf("PID:%i\n",getpid());
    
    //Infinite loop, printing ascii '46'
    char fourtySix;
    fourtySix = 46;
    while(1){
        sleep(1);
        printf("%c",fourtySix);
        fflush(stdout);
    }
}

void getHosts(){

    int fd;
    FILE* fileStream;
    struct flock fl;

    
    //Open file and attempt to read-lock entire file
    fd = open(hostFile,O_RDONLY);
    if(fd == -1){
        printf("Error open() - %s\n",strerror(errno));
        exit(EXIT_FAILURE);    
    }
    
    printf("Attempting to lock entire file:'%s'...\n",hostFile);
    fl.l_type = F_RDLCK;
    fl.l_whence = SEEK_SET;
    fl.l_start = 0;
    fl.l_len = 0;
    
    if(fcntl(fd,F_SETLK,&fl) == -1){
        printf("Error fcntl() - %s\n",strerror(errno));
        exit(EXIT_FAILURE);
    }
    
    //Read in hosts line by line
    if((fileStream = fopen(hostFile,"r")) == NULL){
        printf("Error fopen() - %s\n",strerror(errno));
        exit(EXIT_FAILURE);
    }

    int currLine;char* newLine;size_t size;
    currLine = 0;newLine=NULL;size = 81;
    while(getline(&newLine,&size,fileStream) != -1 ){
        //EXTRA CODE FOR TESTING READ LOCK
        sleep(60);
        newLine[strlen(newLine)-1] = '\0';
        hosts[currLine] = strcpy(hosts[currLine],newLine);
        currLine++;
        numHosts++;
    }
    free(newLine);
    if(!feof(fileStream)){
        printf("Error getLine() - %s\n",strerror(errno));
        exit(EXIT_FAILURE);
    }
    
    //Release read lock
    printf("Releasing read lock\n");
    fl.l_type = F_UNLCK;       
    fcntl(fd,F_SETLKW);
    fclose(fileStream);

    return;
}

void pingHosts(){
    int i;
    int children[numHosts];
    int hostStatus[numHosts];
    
    //Fork a child per host to ping
    groupId = -2;
    for(i=0;i<numHosts;i++){
        char* intCharS = malloc(sizeof(int));
        sprintf(intCharS,"%i",interval);
        children[i] = fork();
        if(children[i] == 0){
            //Execute pingOne
            char* args[3] = {hosts[i],intCharS,NULL};
            if(execvp("./pingOne",args) == -1){
                printf("Error execvp - %s\n",strerror(errno));
                exit(EXIT_FAILURE);
            }
        }else if(children[i] == -1){
            printf("Error fork() - %s\n",strerror(errno));
            exit(EXIT_FAILURE);
        }else{
            if(groupId==-2)
                groupId = children[i];
            if(setpgid(children[i],groupId)==-1){
                printf("Error fork() - %s\n",strerror(errno));
                exit(EXIT_FAILURE);
            }
            free(intCharS);
        }
    }
    
    //Wait for children to finish
    for(i=0;i<numHosts;i++){
        if(waitpid(children[i],&hostStatus[i],0) == -1){
            printf("Error waitpid() - %s\n",strerror(errno));
            exit(EXIT_FAILURE);
        }
        if(hostStatus[i] == 0)
            printf("Child %i  is reaped after pinging host '%s' which is alive.\n",children[i],hosts[i]);
        else
            printf("Child %i  is reaped after pinging host '%s' which is dead.\n",children[i],hosts[i]);
    }
    
    //Write deadHosts
    int fd;
    FILE* fileStream;
    struct flock fl;
    
    //Open file and attempt to read-lock entire file
    if((fileStream = fopen(deadHosts,"w+")) == NULL){
        printf("Error fopen() - %s\n",strerror(errno));
        exit(EXIT_FAILURE);
    }
    
    fd = open(deadHosts,O_WRONLY);
    if(fd == -1){
        printf("Error open() - %s\n",strerror(errno));
        exit(EXIT_FAILURE);    
    }
    
    printf("Attempting to lock entire file:'%s'...\n",deadHosts);
    fl.l_type = F_WRLCK;
    fl.l_whence = SEEK_SET;
    fl.l_start = 0;
    fl.l_len = 0;
    
    if(fcntl(fd,F_SETLK,&fl) == -1){
        printf("Error fcntl() - %s\n",strerror(errno));
        exit(EXIT_FAILURE);
    }
    
    //Write to file
    for(i=0;i<numHosts;i++){
        if(hostStatus[i] != 0){
            if(fprintf(fileStream,"%s\n",hosts[i]) < 0) {printf("Error writing deadHosts\n");exit(EXIT_FAILURE);}
        }    
    }
    fclose(fileStream);
    
    //Release lock
    printf("Releasing write lock\n");
    fl.l_type = F_UNLCK;       
    fcntl(fd,F_SETLKW);
    
    //Re-set alarm
    alarm(interval);
    return;
}

void cleanup(){
    int i;
    for(i=0;i<10;i++){
        free(hosts[i]);
    }
    if(kill(-groupId,SIGTERM) == -1){
        printf("Error kill()\n");
        exit(EXIT_FAILURE);
    }
    exit(EXIT_SUCCESS);
}
