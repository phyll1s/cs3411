#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>

int main(int argc, char *argv[]) {

  struct flock  theLock;
  char          *timeBuf;
  time_t        timePtr;
  int           i;
  int           fd;
  int           sleepInterval;
  
  sleepInterval=atoi(argv[1]);
  fd=open(argv[2],O_RDWR|O_APPEND);

  theLock.l_type=F_WRLCK;     /* Write lock */
  theLock.l_whence=SEEK_SET;  /* Offset from start of file */
  theLock.l_start=0;          /* Region starts at offset zero */
  theLock.l_len=0;            /* Entire file */
 
  fcntl(fd,F_SETLKW,&theLock);
  write(1,"Acquired lock\n",14);
  time(&timePtr);
  timeBuf=ctime(&timePtr);
  for (i=0;i<strlen(timeBuf);i++) {
    sleep(1);
    write(fd,&timeBuf[i],1);
    write(1,"*",1);
  }
  write(fd,"\n",1);
  write(1,"Sleeping\n",9);
  sleep(sleepInterval);
  write(1,"Awake\n",6);
  theLock.l_type=F_UNLCK;
  theLock.l_whence=SEEK_SET;
  theLock.l_start=0;
  theLock.l_len=0;           
  theLock.l_pid=1;
  write(1,"Releasing lock\n",15);
  fcntl(fd,F_SETLK,&theLock);

}
