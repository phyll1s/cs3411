#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>

int  sleepInterval;
char *targetFile;

void synchWrite(int sigNum){
  struct flock  theLock;
  char          *timeBuf;
  time_t        timePtr;
  int           i;
  int           fd;

  fd=open(targetFile,O_RDWR|O_APPEND);

  theLock.l_type=F_WRLCK;     /* Write lock */
  theLock.l_whence=SEEK_SET;  /* Offset from start of file */
  theLock.l_start=0;          /* Region starts at offset zero */
  theLock.l_len=0;            /* Entire file */

  write(1,"Attempting lock\n",16); 
  fcntl(fd,F_SETLKW,&theLock);
  write(1,"Acquired lock\n",14);
  time(&timePtr);
  timeBuf=ctime(&timePtr);
  for (i=0;i<strlen(timeBuf);i++) {
    sleep(1);
    write(fd,&timeBuf[i],1);
    write(1,"*",1);
  }
  write(fd,"\n",1);  write(1,"\n",1);

  write(1,"Sleeping in synchWrite\n",24);
  sleep(sleepInterval);
  write(1,"Awake\n",6);

  theLock.l_type=F_UNLCK;
  theLock.l_whence=SEEK_SET;
  theLock.l_start=0;
  theLock.l_len=0;           
  theLock.l_pid=1;
  write(1,"Releasing lock\n",15);
  fcntl(fd,F_SETLK,&theLock);

  alarm(sleepInterval);
}
void cleanUp(int sigNum){
  write(1,"Exiting\n",8);
  exit(1);
}
main(int argc, char *argv[]){

  sigset_t   newMask;
  sigset_t   oldMask;
  struct sigaction  act;
 
  targetFile=argv[2];
  sleepInterval=atoi(argv[1]);
  printf("Ping interval is %d (sec)\n",sleepInterval);

  /*-------------------
    Block ALRM and TERM
    -------------------
  */
  sigemptyset(&newMask);       
  sigaddset(&newMask,SIGALRM);
  sigaddset(&newMask,SIGTERM);
  sigprocmask(SIG_BLOCK,&newMask,&oldMask);  

  /*------------------
    Install Handlers
    ------------------
  */
  act.sa_handler= synchWrite;
  act.sa_flags  = 0;                /* Default => sig blocked in handler */
  sigemptyset(&act.sa_mask);
  sigaddset(&act.sa_mask,SIGTERM);
  sigaction(SIGALRM,&act,NULL);     /* Now ALARM and TERM blocked in handler */

  act.sa_handler= cleanUp;
  act.sa_flags  = 0;               
  sigemptyset(&act.sa_mask);       
  sigaction(SIGTERM,&act,NULL);

  write(1,"Main sleeping\n",14);
  sleep(sleepInterval);
  write(1,"Main waking\n",12);

  /*-------------------
    Restore signal mask
    -------------------
  */
  sigprocmask(SIG_SETMASK,&oldMask,NULL);  

  /*-------------------
    Set alarm
    -------------------
  */
  alarm(sleepInterval);
  while (1==1) {sleep(1);write(1,".",1);}
}
